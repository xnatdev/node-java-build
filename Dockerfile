#
# node-java-build: Dockerfile
# XNAT https://www.xnat.org
# Copyright (c) 2022, Washington University School of Medicine
# All Rights Reserved
#
# Released under the Simplified BSD.
#

FROM ubuntu:bionic

ARG APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1
ARG DEBIAN_FRONTEND=noninteractive
ARG JDK_ROOT="https://cdn.azul.com/zulu/bin"
ARG JAVA_VERSION="zulu8.64.0.19-ca-jdk8.0.345"
ARG NODE_VERSION=14.20.0
ARG YARN_VERSION=1.22.19

ENV JAVA_HOME_LINUX "/opt/java/linux-x64"
ENV JAVA_HOME_WIN "/opt/java/win-x64"
ENV JAVA_HOME "${JAVA_HOME_LINUX}"
ENV PATH "${JAVA_HOME}/bin:${PATH}"

# node installations command expect to run as root
## Using node installation from https://raw.githubusercontent.com/nodejs/docker-node/3e539e6925a524bf4fda47ea33ed33d0d4fb0e20/10/stretch/Dockerfile
USER root

# Add 32-bit arch, add winehq key and repo, update, and clean up, then install git, winehq, and xz-utils
RUN apt-get update \
  && apt-get --assume-yes --auto-remove full-upgrade \
  && apt-get --assume-yes install --no-install-recommends apt-transport-https g++ git gnupg2 libcurl3 libcurl-openssl1.0-dev make openssh-client python3-pip python3-setuptools software-properties-common sudo unzip wget xz-utils \
  && python3 -m pip install httpie \
  && dpkg --add-architecture i386 \
  && wget -nc -O /usr/share/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key \
  && wget -nc -P /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/bionic/winehq-bionic.sources \
  && apt-get --assume-yes update \
  && apt-get --assume-yes install --install-recommends winehq-stable python2.7 \
  # Add node and circleci users \
  && groupadd --gid 1001 node \
  && useradd --uid 1001 --gid node --shell /bin/bash --create-home node \
  && groupadd --gid 3434 circleci \
  && useradd --uid 3434 --gid circleci --shell /bin/bash --create-home circleci \
  && usermod --append --groups sudo circleci \
  && echo "circleci ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/10-circleci \
  && ARCH= && dpkgArch="$(dpkg --print-architecture)" \
  && case "${dpkgArch##*-}" in \
    amd64) ARCH='x64';; \
    ppc64el) ARCH='ppc64le';; \
    s390x) ARCH='s390x';; \
    arm64) ARCH='arm64';; \
    armhf) ARCH='armv7l';; \
    i386) ARCH='x86';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac \
  && set -ex \
  && wget -q "https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-${ARCH}.tar.xz" \
  && wget -q "https://yarnpkg.com/downloads/${YARN_VERSION}/yarn-v${YARN_VERSION}.tar.gz" \
  && tar -xJf "node-v${NODE_VERSION}-linux-${ARCH}.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
  && tar -xzf yarn-v${YARN_VERSION}.tar.gz -C /usr/local \
  && ln -s /usr/local/bin/node /usr/local/bin/nodejs \
  && ln -s /usr/local/yarn-v${YARN_VERSION}/bin/yarn /usr/local/bin/yarn \
  && ln -s /usr/local/yarn-v${YARN_VERSION}/bin/yarnpkg /usr/local/bin/yarnpkg \
  && rm "node-v${NODE_VERSION}-linux-${ARCH}.tar.xz" yarn-v${YARN_VERSION}.tar.gz \
  # Download JREs for building app: using Zulu JRE builds to avoid Oracle licensing issues. \
  && mkdir -p /opt/java \
  && wget -q ${JDK_ROOT}/${JAVA_VERSION}-win_x64.zip \
  && wget -q ${JDK_ROOT}/${JAVA_VERSION}-linux_x64.tar.gz \
  && unzip -qqd /opt/java ${JAVA_VERSION}-win_x64.zip \
  && tar -xf ${JAVA_VERSION}-linux_x64.tar.gz -C /opt/java \
  && rm -f zulu8.* \
  && mv /opt/java/${JAVA_VERSION}-win_x64 /opt/java/win-x64 \
  && mv /opt/java/${JAVA_VERSION}-linux_x64 /opt/java/linux-x64 \
  && chown -R circleci:circleci /opt/java \
  && apt-get --assume-yes autoremove \
  && rm -rf /var/lib/apt/lists/*

USER circleci

