#!/usr/bin/env groovy
/*
 * node-java-build: Jenkinsfile
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

pipeline {
    environment {
        registry = "xnat/node-java-build"
        registryCredential = "xnat-docker"
        dockerImage = ""
    }
    agent any
    stages {
        stage("Build node-java-build Docker image") {
            steps{
                script {
                    echo "Building node-java-build Docker image from branch ${BRANCH_NAME}: build #${BUILD_NUMBER}"
                    dockerImage = docker.build registry + ":${BUILD_NUMBER}" + (BRANCH_NAME == "master" ? "" : "-${BRANCH_NAME}")
                }
            }
        }
        stage("Deploy image to Docker Hub") {
            steps{
                script {
                    echo "Deploying \"${registry}\" image to Docker Hub from branch ${BRANCH_NAME} build #${BUILD_NUMBER}"
                    docker.withRegistry("", registryCredential ) {
                        dockerImage.push()
                    }
                }
            }
        }
        stage("Tag deployed image as latest when built from master branch") {
            when {
                expression { BRANCH_NAME == "master" }
            }
            steps{
                script {
                    echo "Tagging \"${registry}\" image from branch ${BRANCH_NAME} build #${BUILD_NUMBER} with latest tag on Docker Hub"
                    docker.withRegistry("", registryCredential ) {
                        dockerImage.push("latest")
                    }
                }
            }
        }
    }
}
